pdf-parser (0.7.10-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.10
  * Bump Standards-Version to 4.7.0 (no change)
  * Add new dep python3-pyzipper
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 05 Nov 2024 17:49:31 +0100

pdf-parser (0.7.9-0kali2) kali-dev; urgency=medium

  [ X0RW3LL ]
  * Fix Python 3.12 Syntax Warnings (use raw strings)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Jul 2024 15:40:33 +0200

pdf-parser (0.7.9-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * d/upstream: Add upstream metadata

  [ Sophie Brun ]
  * New upstream version 0.7.9

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 06 Jun 2024 11:33:23 +0200

pdf-parser (0.7.8-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.8

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 14 Feb 2023 12:06:23 +0100

pdf-parser (0.7.7-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.7

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 10 Nov 2022 15:53:45 +0100

pdf-parser (0.7.6-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Update email address
  * Add Uploaders
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * New upstream version 0.7.6
  * Update debian/watch
  * Bump Standards-Version to 4.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 30 May 2022 14:03:17 +0200

pdf-parser (0.7.5-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.5
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 19 Aug 2021 09:32:07 +0200

pdf-parser (0.7.4-0kali2) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.5.0, no changes needed.

  [ Sophie Brun ]
  * Update maximum python3 version in patch: auto-generated dependency was on
    Python2 instead of Python3

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 16 Sep 2020 09:27:11 +0200

pdf-parser (0.7.4-0kali1) kali-dev; urgency=medium

  * New upstream version 0.7.4

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 03 Jan 2020 10:18:33 +0100

pdf-parser (0.7.3-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0.7.3
  * Refresh switch to Python 3

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 02 Oct 2019 17:09:14 +0200

pdf-parser (0.7.2-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0.7.2
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.4.0
  * Switch to Python 3
  * Add minimal autopkgtest

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Aug 2019 15:55:07 +0200

pdf-parser (0.6.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 03 Feb 2016 17:12:29 +0100

pdf-parser (0.4.3-1kali2) kali-dev; urgency=medium

  * Drop dependency on python-support (obsolete package)
  * Use debhelper 9
  * Add a debian/watch

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Jan 2016 15:51:39 +0100

pdf-parser (0.4.3-1kali1) kali; urgency=low

  * Update to 0.4.3

 -- Emanuele Acri <crossbower@kali.org>  Mon, 23 Sep 2013 14:29:22 +0200

pdf-parser (0.3.9-1kali0) kali; urgency=low

  * Initial release

 -- Devon Kearns <dookie@kali.org>  Mon, 07 Jan 2013 07:11:17 -0700
